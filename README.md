# VHT-REST-CLIENT-PHP #

Example code for the VHT RESTful API
====================================

Introduction
------------

### Features ###

This library's features include the following.

* [Airtime](https://bitbucket.org/vhtvn/vht-rest-client-php/overview#markdown-header-airtime)
* [ZMS](https://bitbucket.org/vhtvn/vht-rest-client-php/overview#markdown-header-zms)
* [SMS](https://bitbucket.org/vhtvn/vht-rest-client-php/overview#markdown-header-sms)
* 

Installation
------------

This library is available on [Packagist](http://packagist.org/packages/vhtvn/vht-rest-client). 
The recommended way to install this library is through [Composer](http://getcomposer.org):

```bash
$ php composer.phar require vhtvn/vht-rest-client dev-master
```

Usage
-----

Before testing code below, we must require php file "autoload.php"
```bash
require(BASE_DIR . 'vendor/autoload.php');
```

### Airtime ###

```php
$apiKey       = ''; // your apikey
$apiSecretKey = ''; // your apisecret
$mac          = \Vht\Common\VhtHelper::buildMacForAuthentication(array($apiKey, $apiSecretKey));
$airtime      = new \Vht\Airtime($apiKey, $mac);

$balance      = $airtime->get('getbalances');
if ($airtime->getLastResponse()['headers']['http_code'] === 200) {
    echo "Success\n";
    print_r($balance);
} else {
    echo "Fail\n";
    print_r($airtime->getLastRequest());
    print_r($airtime->getLastResponse());
    print_r($balance);
}

--
$params = array(
    'action' => $action,
    'apikey' => $apiKey,
    'servicecode' => $serviceCode,
    'account' => $account,
    'amount' => $amount,
    'trace' => $trace,
    'timestamp' => $timeStamp,
);
$mac          = \Vht\Common\VhtHelper::buildMacForAuthentication(array($action, $apiKey, $serviceCode, $account, $amount, $trace, $timeStamp, $apiSecretKey));
$airtime      = new \Vht\Airtime($apiKey, $mac);

$topup  = $airtime->get('topups', $params);
if ($airtime->getLastResponse()['headers']['http_code'] === 200) {
    echo "Success\n";
    print_r($topup);
} else {
    echo "Fail\n";
    print_r($airtime->getLastRequest());
    print_r($airtime->getLastResponse());
    print_r($topup);
}
```


### ZMS ###
```php
$apiKey       = ''; // your apikey
$apiSecretKey = ''; // your apisecret
$zmsObj       = new \Vht\Zms($apiKey, $apiSecretKey);

$params = array(
    'phone' => '',
    'message' => 'The content of the message',
    'sms' => 'Content of SMS message',
    'is_notify' => true,
);

$result = $zmsObj->post('zalophones/text', $params);
if ($zmsObj->getLastResponse()['headers']['http_code'] === 201) {
    echo "Success\n";
} else {
    echo "Fail\n";
    print_r($zmsObj->getLastRequest());
    print_r($zmsObj->getLastResponse());
    print_r($result);
}
```


### SMS ###
```php
$apiKey    = ''; // your apikey
$apiSecret = ''; // your apisecret
$brandName = new \Vht\SmsBrandName($apiKey);
//$brandName->verifySsl = false;

$params['submission'] = array(
    'api_key' => $apiKey,
    'api_secret' => $apiSecret,
    'sms' => array(
        array(
            'id' => (string) \Ramsey\Uuid\Uuid::uuid4(),
            'brandname' => 'VHT',
            'text' => 'Form VHT with love',
            'to' => '' //your phone 0xxx
        )
    )
);

$result = $brandName->post('ccsms', ($params));
if ($brandName->getLastResponse()['headers']['http_code'] === 200) {
    echo "Success\n";
} else {
    echo "Fail\n";
    print_r($brandName->getLastRequest());
    print_r($brandName->getLastResponse());
    print_r($brandName->getLastError());
    print_r($result);
}
```